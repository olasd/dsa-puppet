class nagios::server {
	include apache2
	include apache2::ssl
	include apache2::authn_anon

	ssl::service { 'nagios.debian.org':
		notify  => Exec['service apache2 reload'],
		key => true,
	}
	apache2::site { "10-nagios.debian.org":
		site => "nagios.debian.org",
		content => template('nagios/nagios.debian.org.conf.erb'),
	}

	ensure_packages( [
		'icinga',
		'make',
		'monitoring-plugins',
		'nagios-nrpe-plugin',
		], { ensure => 'installed' })

	service { 'icinga':
		ensure => running,
		require => Package['icinga'],
	}

	file { '/etc/icinga/apache2.conf':
		content => template('nagios/icinga-apache2.conf.erb'),
		notify  => Exec['service apache2 reload'],
	}
	file { '/srv/nagios.debian.org/htpasswd':
		mode => '0640',
		owner => 'root',
		group => 'www-data',
	}
	file { '/etc/icinga/cgi.cfg':
		ensure => symlink,
		target => 'config-pushed/static/cgi.cfg',
		notify  => Exec['service apache2 reload'],
	}
	file { '/etc/icinga/icinga.cfg':
		ensure => symlink,
		target => 'config-pushed/static/icinga.cfg',
		notify  => Service['icinga'],
	}
	file { '/etc/icinga/objects':
		ensure  => directory,
		mode    => '755',
		purge   => true,
		recurse => true,
		force   => true,
		source  => "puppet:///files/empty/",
		notify  => Service['icinga'],
	}
	file { '/etc/icinga/objects/contacts.cfg':
		ensure => symlink,
		target => '../config-pushed/static/objects/contacts.cfg',
		notify  => Service['icinga'],
	}
	file { '/etc/icinga/objects/generic-host.cfg':
		ensure => symlink,
		target => '../config-pushed/static/objects/generic-host.cfg',
		notify  => Service['icinga'],
	}
	file { '/etc/icinga/objects/generic-service.cfg':
		ensure => symlink,
		target => '../config-pushed/static/objects/generic-service.cfg',
		notify  => Service['icinga'],
	}
	file { '/etc/icinga/objects/timeperiods.cfg':
		ensure => symlink,
		target => '../config-pushed/static/objects/timeperiods.cfg',
		notify  => Service['icinga'],
	}

	file { '/etc/icinga/objects/xauto-dependencies.cfg':
		ensure => symlink,
		target => '../config-pushed/generated/auto-dependencies.cfg',
		notify  => Service['icinga'],
	}
	file { '/etc/icinga/objects/xauto-hostgroups.cfg':
		ensure => symlink,
		target => '../config-pushed/generated/auto-hostgroups.cfg',
		notify  => Service['icinga'],
	}
	file { '/etc/icinga/objects/xauto-hosts.cfg':
		ensure => symlink,
		target => '../config-pushed/generated/auto-hosts.cfg',
		notify  => Service['icinga'],
	}
	file { '/etc/icinga/objects/xauto-servicegroups.cfg':
		ensure => symlink,
		target => '../config-pushed/generated/auto-servicegroups.cfg',
		notify  => Service['icinga'],
	}
	file { '/etc/icinga/objects/xauto-services.cfg':
		ensure => symlink,
		target => '../config-pushed/generated/auto-services.cfg',
		notify  => Service['icinga'],
	}

	file { '/etc/nagios-plugins/config/local-dsa-checkcommands.cfg':
		ensure => symlink,
		target => '../../icinga/config-pushed/static/checkcommands.cfg',
		notify  => Service['icinga'],
	}
	file { '/etc/nagios-plugins/config/local-dsa-eventhandlers.cfg':
		ensure => symlink,
		target => '../../icinga/config-pushed/static/eventhandlers.cfg',
		notify  => Service['icinga'],
	}

	file { '/etc/icinga/config-pushed':
		ensure => symlink,
		target => '/srv/nagios.debian.org/config-pushed'
	}

	file { '/srv/nagios.debian.org':
		ensure => directory,
		mode => '0755',
	}
	file { '/srv/nagios.debian.org/config-pushed':
		ensure => directory,
		mode => '0755',
		owner  => 'nagiosadm',
		group  => 'nagiosadm',
	}

	concat::fragment { 'dsa-puppet-stuff--nagios--restart-stale-icinga':
		target => '/etc/cron.d/dsa-puppet-stuff',
		order  => '010',
		content  => @(EOF)
			*/15 * * * * root find /var/lib/icinga/status.dat -mmin +20 | grep -q . && service icinga restart
			| EOF
	}
}
