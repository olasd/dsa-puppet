class systemd {
	file { '/etc/systemd/journald.conf':
		source => 'puppet:///modules/systemd/journald.conf',
	}

	systemd::mask { 'sys-kernel-debug-tracing.mount': }
	systemd::mask { 'sys-kernel-debug.mount': }

	if $::hostname == 'godard' {
		exec {'mkdir -p /etc/systemd/journald.conf.d':
			unless => 'test -d /etc/systemd/journald.conf.d',
		}
		file { '/etc/systemd/journald.conf.d/persistency.conf':
			source => 'puppet:///modules/systemd/persistency.conf',
		}
	}

	file { '/usr/local/sbin/systemd-cleanup-failed':
		source => 'puppet:///modules/systemd/systemd-cleanup-failed',
		mode   => '0555',
	}
	concat::fragment { 'dsa-puppet-stuff--systemd-cleanup-failed':
		target => '/etc/cron.d/dsa-puppet-stuff',
		content  => @("EOF"),
				*/10 * * * * root /usr/local/sbin/systemd-cleanup-failed
				| EOF
	}
}
