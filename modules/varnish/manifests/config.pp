define varnish::config (
	$listen = [':6081'],
	$source=undef,
	$content=undef,
	$ensure = 'present',
	$backend = "-s malloc,256m",
) {
	if $name != "default" {
		fail ( "This module cannot setup non-default varnish instances yet." )
	}

	case $ensure {
		present: {
			include varnish::base
			include stretch::network_online

			if ! ($source or $content) {
				fail ( "No configuration found for ${name}" )
			}

			$listenarr = [] + $listen
			$listenstr = join(prefix($listenarr, "-a "), " ")
			systemd::override { 'varnish':
				content  => @("EOF"),
					[Unit]
					After=network-online.target
					[Service]
					ExecStart=
					ExecStart=/usr/sbin/varnishd -j unix,user=vcache -F ${listenstr} -T localhost:6082 -f /etc/varnish/${name}.vcl -S /etc/varnish/secret -s ${backend}
					| EOF
			}

			$dest = "/etc/varnish/${name}.vcl"
			if $content {
				file { "${dest}":
					ensure  => $ensure,
					content => $content,
					notify  => Service["varnish"],
				}
			} elsif $source {
				file { "${dest}":
					ensure  => $ensure,
					source  => $source,
					notify  => Service["varnish"],
					}
			}
		}
		default: {
			fail ( "Can only deal with ensure=>present for now" )
		}
	}
}
