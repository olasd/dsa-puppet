
- static-update-component is run by the user on the source host.
  If not run under sudo as the staticuser already, it sudos to the staticuser,
  re-execing itself.  It them sshs to the static-master for that component to
  run static-master-update-component.

  LOCKING:
    none, but see static-master-update-component

- static-master-update-component is run on the static-master.
  It rsyncs the contents from the source host to the static master, and then
  triggers static-master-run to push the content to the mirrors.

  The sync happens to a new <component>-updating.incoming-XXXXXX directory.  On
  sync success, <component> is replaced with that new tree, and the
  static-master-run trigger happens.

  LOCKING:
    - exclusive locks are held on
      - <component>.lock

- static-master-run triggers all the mirrors for a component to initiate syncs.
  When all mirrors have an up-to-date tree, they are instructed to update
  the cur-> symlink to the new tree.

  To begin with, static-master-run copies <component> to <component>-current-push.
  This is the tree all the mirrors then sync from.  If the push was successful,
  <component>-current-push is renamed to <component>-current-live.

  LOCKING:
    - exclusive locks are held on
      - <component>.lock

- static-mirror-run runs on a mirror and syncs components.
  There is a symlink called 'cur' that points to either tree-a or tree-b for
  each component.  the cur tree is the one that is live, the other one usually
  does not exist, except when a sync is ongoing (or a previous one failed and
  we keep a partial tree).

  During a sync, we sync to the tree-<X> that is not the live one.  When instructed by
  static-master-run, we update the symlink and remove the old tree.

  static-mirror-run rsyncs either -current-push or -current-live for a component.

  LOCKING:
    during all of static-mirror-run, we keep an exclusive lock on the <component>
    dir, i.e., the directory that holds tree-[ab] and cur.

- static-mirror-run-all

  Run static-mirror-run for all components on this mirror, fetching the -live- tree.

  LOCKING:
    none, but see static-mirror-run.

- staticsync-ssh-wrap

  wrapper for ssh job dispatching on source, master, and mirror.

  LOCKING:
    - on master, when syncing -live- trees:
        a shared lock is held on <component>.lock during
        the rsync process.
