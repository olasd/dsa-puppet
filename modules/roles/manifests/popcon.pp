class roles::popcon {
	ssl::service { 'popcon.debian.org':
		notify  => Exec['service apache2 reload'],
		key => true,
	}

	include apache2::ssl
	apache2::site { 'popcon.debian.org':
		site => 'popcon.debian.org',
		source => 'puppet:///modules/roles/popcon/popcon.debian.org.conf',
	}
}
