class roles::snapshot_web {
	include apache2
	include apache2::rewrite

	# snapshot abusers
	#  61.69.254.110 - 20180705, mirroring with wget
	@ferm::rule { 'dsa-snapshot-abusers':
		prio  => "000",
		rule  => "saddr (61.69.254.110) DROP",
	}

	ensure_packages ( [
		"libapache2-mod-wsgi",
		], {
		ensure => 'installed',
	})

	apache2::site { '020-snapshot.debian.org':
		site   => 'snapshot.debian.org',
		content => template('roles/snapshot/snapshot.debian.org.conf.erb')
	}

	case $::hostname {
		'lw07': {
			$ipv4addr        = '185.17.185.185'
			$ipv6addr        = '2001:1af8:4020:b030:deb::185'
			$ipv6addr_apache = '2001:1af8:4020:b030:deb::187'
		}
		'sallinen': {
			$ipv4addr        = '193.62.202.27'
			$ipv6addr        = '2001:630:206:4000:1a1a:0:c13e:ca1b'
			$ipv6addr_apache = '2001:630:206:4000:1a1a:0:c13e:ca1a'
		}
		default: {
			fail ( "unknown host $::hostname for snapshot_web." )
		}
	}

	# varnish cache
	###############
	@ferm::rule { 'dsa-nat-snapshot-varnish-v4':
		table => 'nat',
		chain => 'PREROUTING',
		rule  => "proto tcp daddr ${ipv4addr} dport 80 REDIRECT to-ports 6081",
	}

	varnish::config { 'default':
		listen  => [
			':6081',
			"[$ipv6addr]:80"
			],
		backend => 'file,/var/lib/varnish/varnish_storage.bin,8G',
		content => template('roles/snapshot/snapshot.debian.org.vcl.erb'),
	}

	# the ipv6 port 80 is owned by varnish
	file { '/etc/apache2/ports.conf':
		content  => @("EOF"),
			Listen 0.0.0.0:80
			Listen [$ipv6addr_apache]:80
			| EOF
		require => Package['apache2'],
		notify  => Service['apache2'],
	}

	# haproxy ssl termination
	#########################
	include haproxy
	file { '/etc/haproxy/haproxy.cfg':
		content => template('roles/snapshot/haproxy.cfg.erb'),
		require => Package['haproxy'],
		notify  => Service['haproxy'],
	}
	ssl::service { 'snapshot.debian.org':
		notify  => Service['haproxy'],
		key => true,
	}
}
